from __future__ import print_function
import re
import zipfile
import sys

from htmlextractor.extractor import Extractor, MissingWordsException, MalformedWordsException

from util.logger import log_with, root_logger
from util.util import DbConnector

data_path_prefix = '../ed-data/'


class DbImporter(DbConnector):
    def __init__(self, dbName):
        super(DbImporter, self).__init__(dbName)
        self.postfix = r'log.html'
        self.prefix_pattern = re.compile(
            r'^tenbyten.org/Data/global/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<hour>\d{2})/$')
        #r'^tenbyten.org/Data/global/(?P<year>2010)/(?P<month>07)/(?P<day>01)/(?P<hour>06)/$')

    def create_indexes(self, cursor):
        indexes_sql = {
            'words_word_idx': "CREATE INDEX IF NOT EXISTS words_word_idx ON words (word)",
            'source_name_idx': "CREATE INDEX IF NOT EXISTS source_name_idx ON source (name)",
            'article_source_id_fk_idx': "CREATE INDEX IF NOT EXISTS article_source_id_fk_idx ON article (source_id)",
            'article_link_idx': "CREATE INDEX IF NOT EXISTS article_link_idx ON article (link)",
            'hour_words_word_id_fk_idx': "CREATE INDEX IF NOT EXISTS hour_words_word_id_fk_idx ON hour_words (word_id)",
            'word_articles_hour_words_id_fk_idx': "CREATE INDEX IF NOT EXISTS word_articles_hour_words_id_fk_idx ON word_articles (hour_word_id)",
            'word_articles_article_id_fk_idx': "CREATE INDEX IF NOT EXISTS word_articles_article_id_fk_idx ON word_articles (article_id)",
            'hour_words_year_idx': "CREATE INDEX IF NOT EXISTS hour_words_year_idx ON hour_words (year)",
            'hour_words_month_idx': "CREATE INDEX IF NOT EXISTS hour_words_month_idx ON hour_words (month)",
            'hour_words_day_idx': "CREATE INDEX IF NOT EXISTS hour_words_day_idx ON hour_words (day)",
            'hour_words_hour_idx': "CREATE INDEX IF NOT EXISTS hour_words_hour_idx ON hour_words (hour)",
            'hour_words_date_idx': "CREATE INDEX IF NOT EXISTS hour_words_date_idx ON hour_words (year, month, day, hour)"
        }
        for name, sql in indexes_sql.items():
            try:
                root_logger.info('begin - ' + name)
                cursor.execute(sql)
                self.connection.commit()
                root_logger.info('end - ' + name)
            except Exception as e:
                root_logger.exception(e)

    @log_with(root_logger)
    def createTables(self):
        cursor = self.connection.cursor()
        cursor.execute('''
                        CREATE TABLE IF NOT EXISTS words (
                            id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            word  VARCHAR(255) NOT NULL UNIQUE);
                      ''')
        cursor.execute('''
                         CREATE TABLE IF NOT EXISTS source (
                            id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            name  VARCHAR(255) NOT NULL UNIQUE);
                      ''')
        cursor.execute('''
                          CREATE TABLE IF NOT EXISTS article (
                            id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            source_id   INTEGER(10) NOT NULL,
                            link        VARCHAR(255) NOT NULL,
                          FOREIGN KEY(source_id) REFERENCES source(id));
                      ''')
        cursor.execute('''
                        CREATE TABLE IF NOT EXISTS hour_words (
                            id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            word_id         INTEGER(10) NOT NULL,
                            rank            INTEGER(2) NOT NULL,
                            appearances     INTEGER(10) NOT NULL,
                            perc_word_total REAL(10) NOT NULL,
                            points          REAL(10) NOT NULL,
                            year            INTEGER(2) NOT NULL,
                            month           INTEGER(1) NOT NULL,
                            day             INTEGER(1) NOT NULL,
                            hour            INTEGER(1) NOT NULL,
                        FOREIGN KEY(word_id) REFERENCES words(id));
                      ''')
        cursor.execute('''
                        CREATE TABLE IF NOT EXISTS word_articles (
                            id            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            hour_word_id  INTEGER(10) NOT NULL,
                            article_id    INTEGER(10) NOT NULL,
                            local_article_id    INTEGER(10) NOT NULL,
                        FOREIGN KEY(hour_word_id) REFERENCES hour_words(id),
                        FOREIGN KEY(article_id) REFERENCES article(id));
                      ''')
        self.create_indexes(cursor)
        pass

    @log_with(root_logger)
    def loadData(self, zip_file_name):
        with zipfile.ZipFile(zip_file_name, "r") as f:
            with open('../logs/notParsed.log', 'w') as not_parsed_file:
                last_day = -1
                for file_name in f.namelist():
                    matcher = self.prefix_pattern.match(file_name)
                    if matcher:
                        year = matcher.group('year')
                        month = matcher.group('month')
                        day = matcher.group('day')
                        hour = matcher.group('hour')
                        if (day != last_day):
                            root_logger.info('Processing file: "%s"' % (file_name, ))
                        last_day = day
                        try:
                            log_html = str(f.read(file_name + self.postfix))
                            html_extractor = Extractor(log_html)
                            result = html_extractor.extract_data()
                            self._insert_record(result, year, month, day, hour)
                        except MissingWordsException as e:
                            root_logger.error('Error while processing file {}:\n{}'.format(file_name, e))
                            not_parsed_file.write(file_name + ' - ' + str(e) + '\n')
                        except MalformedWordsException as e:
                            root_logger.error('Error while processing file {}:\n{}'.format(file_name, e))
                            not_parsed_file.write(file_name + ' - ' + str(e) + '\n')
                        except Exception as e:
                            root_logger.exception(e)
                            not_parsed_file.write(file_name + ' - ' + str(e) + '\n')
                        pass
        self.connection.commit()

    def _insert_record(self, record, year, month, day, hour):
        # insert sources
        source_db_ids = {}
        for source_name, old_source_id in record.sources.items():
            db_source_id = self._insert_source(source_name)
            source_db_ids[old_source_id] = db_source_id
            pass
            # insert articles
        article_db_ids = {}
        for article_id, value in record.articles.items():
            source_db_id = source_db_ids[value[0]]
            article_db_id = self._insert_article(source_db_id, value[1])
            article_db_ids[article_id] = article_db_id
            pass
            # insert words
        for (word, word_entry) in record.words.items():
            word_id = self._insert_word(word)
            cursor = self.connection.cursor()
            cursor.execute(
                'INSERT INTO hour_words (year, month, day, hour, rank, appearances, perc_word_total, points, word_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)',
                (year, month, day, hour, word_entry.rank, word_entry.appearances, word_entry.perc_word_total,
                 word_entry.points, word_id))
            hour_word_id = cursor.lastrowid
            for article_id in word_entry.article_ids:
                self.connection.execute(
                    'INSERT INTO word_articles (hour_word_id, article_id, local_article_id) VALUES (?, ?, ?)',
                    (hour_word_id, article_db_ids[article_id], article_id))

    def _insert_word(self, word):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM words WHERE word = ?", (word,))
        id = cursor.fetchone()
        if not id:
            cursor.execute('INSERT INTO words (word) VALUES (?)', (word,))
            return cursor.lastrowid
        else:
            return id[0]

    def _insert_source(self, source_name):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM source WHERE name = ?", (source_name,))
        id = cursor.fetchone()
        if not id:
            cursor.execute('INSERT INTO source (name) VALUES (?)', (source_name,))
            return cursor.lastrowid
        else:
            return id[0]

    def _insert_article(self, source_id, article_link):
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM article WHERE link = ?", (article_link,))
        id = cursor.fetchone()
        if not id:
            cursor.execute('INSERT INTO article (source_id, link) VALUES (?, ?)', (source_id, article_link,))
            return cursor.lastrowid
        else:
            return id[0]

    @log_with(root_logger)
    def drop_indexes_and_vacum(self):
        index_list_query = "SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;"
        cursor = self.connection.execute(index_list_query)
        index_names = map(lambda t: t[0], cursor.fetchall())
        root_logger.info('found {} indexes'.format(len(index_names)))
        for index_name in index_names:
            root_logger.info('dropping index: {}'.format(index_name))
            try:
                self.connection.execute('drop index {0:s}'.format(index_name))
            except Exception as e:
                root_logger.exception(e)
        self.connection.execute('VACUUM')
        self.connection.commit()

    pass


def main(argv):
    db_name = '{}tenbyten.sqlite'.format(data_path_prefix)
    dbImporter = DbImporter(db_name)

    dbImporter.createTables()
    #data_zip = '{}tenbyten.org.zip'.format(data_path_prefix)
    #dbImporter.loadData(data_zip)
    # dbImporter.drop_indexes_and_vacum()
    dbImporter.disconnect()


if __name__ == "__main__":
    main(sys.argv)