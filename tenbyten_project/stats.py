from __future__ import print_function
import calendar
import sys
# import matplotlib.pyplot as plt
# from numpy import arange
from util.util import DbConnector

from util.logger import log_with, root_logger

data_path_prefix = '../ed-data/'


class DbImporter(DbConnector):
    @log_with(root_logger)
    def generate_missing_data_plot(self):
        years = [[2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]]
        missing_sum = 0
        expected_sum = 0
        for year_group in years:
            query = 'select year, month, count(word_id) from hour_words where year in ({0:s}) group by year, month order by year asc, month asc'.format(
                ', '.join(map(str, year_group)))
            cursor = self.connection.execute(query)
            result = cursor.fetchone()
            diffs = []
            ylabels = []
            while result:
                year = result[0]
                month = result[1]
                expected = calendar.monthrange(year, month)[1] * 24
                expected_sum += expected
                diff = expected - result[2] / 100
                diffs.append(diff)
                result = cursor.fetchone()
                label = '{}/{}'.format(month, str(year)[2:])
                print(label)
                ylabels.append(str(label))
                print(label)
            missing_sum += sum(diffs)
            p1 = plt.bar(arange(len(diffs)), diffs, 0.5, color='green', align='center', orientation='horizontal')
            print(year_group)
            tmp = ', '.join(map(str, year_group))
            plt.title('Number of missing records for {0:s}'.format(tmp))
            plt.ylabel('Months')
            plt.xlabel('Missing records')
            plt.yticks(arange(len(ylabels)), ylabels)

            #for i in range(len(diffs)):
            #    if diffs[i]:
            #        plt.annotate(str(diffs[i]), xy=(i, 1), horizontalalignment='center',
            #                     verticalalignment='bottom', rotation='vertical', color='black')
            plt.savefig('output/images/{0:s}.png'.format('-'.join(map(str, year_group))))
            plt.show()
        root_logger.info('missing_sum = {}, expected_sum = {}'.format(missing_sum, expected_sum))

    @log_with(root_logger)
    def print_stats(self):
        years = map(str, [2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013])
        print(years)
        queries_map = {
            'indexes': "SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;",
            # 'top_5_words': "SELECT w.word, count(hw.word_id) as c FROM words as w inner join hour_words as hw on w.id = hw.word_id group by hw.word_id order by c desc limit 5",
            # 'top_100_articles': "SELECT a.id, a.link, count(a.id) as c FROM article as a inner join hour_words as hw on a.id = hw.article_id group by hw.article_id order by c desc limit 100",
            # 'words_count_by_year': 'select year, count(word_id) from hour_words group by year order by year',
            # 'sources_words_count': """SELECT source.id , source.name , count( hour_words.id ) AS record_count
            #                        FROM source INNER JOIN article ON source.id = article.source_id
            #                        INNER JOIN word_articles ON article.id = word_articles.article_id
            #                        INNER JOIN hour_words ON hour_words.word_id = word_articles.hour_word_id
            #                        where year in ({})
            #                        GROUP BY source.id
            #                        ORDER BY record_count DESC""".format(', '.join(years)),
            # 'sources_top_words' : 'select source.name, words.word, count(words.id) as occurences from source inner join article on (source.id=article.source_id) inner join word_articles on (article.id=word_articles.article_id) inner join hour_words on (word_articles.hour_word_id=hour_words.id) inner join words on (hour_words.word_id=words.id) group by source.name, words.word order by source.name asc'
        }
        for year in years:
            # queries_map['sources_words_count_in_{}'.format(year)] = """SELECT source.id , source.name , count( hour_words.id ) AS record_count
            #                                 FROM source INNER JOIN article ON source.id = article.source_id
            #                                 INNER JOIN word_articles ON article.id = word_articles.article_id
            #                                 INNER JOIN hour_words ON hour_words.word_id = word_articles.hour_word_id
            #                                 where year = {}
            #                                 GROUP BY source.id
            #                                 ORDER BY record_count DESC""".format(year)
            #  queries_map['top_5_words_in_{}'.format(year)] = "SELECT w.word, count(hw.word_id) as c FROM words as w inner join hour_words as hw on w.id = hw.word_id where hw.year = {} group by hw.word_id order by c desc limit 5".format(year)
            pass
        with open('output/out.txt', 'w') as output:
            table_list_query = "SELECT name FROM sqlite_master WHERE type='index' ORDER BY name;",
            cursor = self.connection.execute(table_list_query)
            table_names = map(lambda t: t[0], cursor.fetchall())
            print(''.ljust(80, '-'), file=output)
            print('\t{0}'.format('rows in tables'), file=output)
            for table_name in table_names:
                rows = self.connection.execute('select count(*) from {0:s}'.format(table_name)).fetchone()[0]
                print('||{0:s}||{1:,d}||'.format(table_name, rows), file=output)

            for name, query in queries_map.items():
                print(name, query)
                print(''.ljust(80, '-'), file=output)
                print('{0}'.format(name), file=output)
                cursor = self.connection.execute(query)
                result = cursor.fetchone()
                while result:
                    row = '||'.join(map(str, result))
                    print('||{0:s}||'.format(row), file=output)
                    result = cursor.fetchone()
                output.flush()
            print(''.ljust(80, '-'), file=output)
        pass


def main(argv):
    db_name = '{}tenbyten.sqlite'.format(data_path_prefix)
    dbImporter = DbImporter(db_name)

    # dbImporter.print_stats()
    # dbImporter.generate_missing_data_plot()
    dbImporter.disconnect()


if __name__ == "__main__":
    main(sys.argv)