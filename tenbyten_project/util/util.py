import sqlite3


class DbConnector(object):
    def __init__(self, dbName):
        self.connection = sqlite3.connect(dbName)

    def disconnect(self):
        self.connection.close()