import functools
import logging


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class log_with(object):
    '''Logging decorator that allows you to log with a
specific logger.
'''
    # Customize these messages
    ENTRY_MESSAGE = 'Entering {}'
    EXIT_MESSAGE = 'Exiting {}'

    def __init__(self, logger=None):
        self.logger = logger

    def __call__(self, func):
        '''Returns a wrapper that wraps func.
The wrapper will log the entry and exit points of the function
with logging.INFO level.
'''
        # set logger if it was not set earlier
        if not self.logger:
            logging.basicConfig()
            self.logger = logging.getLogger(func.__module__)

        @functools.wraps(func)
        def wrapper(*args, **kwds):
            self.logger.info(
                self.ENTRY_MESSAGE.format(func.__name__))  # logging level .info(). Set to .debug() if you want to
            f_result = func(*args, **kwds)
            self.logger.info(
                self.EXIT_MESSAGE.format(func.__name__))   # logging level .info(). Set to .debug() if you want to
            return f_result

        return wrapper


def create_logger():
    global data_path_prefix, log_formatter, root_logger, all_handler, warn_handler, console_handler
    data_path_prefix = '../ed-data/'
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    all_handler = logging.FileHandler("{0}/{1}.log".format('../logs', 'all'), mode='w')
    all_handler.setFormatter(log_formatter)
    root_logger.addHandler(all_handler)
    warn_handler = logging.FileHandler("{0}/{1}.log".format('../logs', 'warn'), mode='w')
    warn_handler.setFormatter(log_formatter)
    warn_handler.setLevel(logging.WARN)
    root_logger.addHandler(warn_handler)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    return root_logger


root_logger = create_logger()