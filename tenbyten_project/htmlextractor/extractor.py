import urlparse
import sys

from bs4 import BeautifulSoup

data_path_prefix = '../ed-data/'


class MissingWordsException(Exception):
    def __init__(self, actual_len):
        """

        @param actual_len:
        """
        self.actual_len = actual_len

    def __str__(self):
        return 'Expected 100 words, given {}'.format(self.actual_len)

    pass


class MalformedWordsException(Exception):
    def __init__(self, word, article_id):
        """

        @param article_id:
        @param word:
        """
        self.word = word
        self.article_id = article_id

    def __str__(self):
        return 'Invalid articles for word {}: {}'.format(self.word, self.article_id)

    pass


class Extractor(object):
    def __init__(self, html_doc):
        """

        @param html_doc:
        """
        self.document = BeautifulSoup(html_doc)
        self.result = Result()

    def extract_data(self):
        """


        @return:
        """
        self._handle_articles()
        self._handle_words()
        self.result.validate()
        return self.result

    def _handle_articles(self):
        for tag in self.document.find_all('a'):
            link = tag['href'].strip(r"\'")
            self.result.add_article(link)

    def _handle_words(self):
        # skip header
        for tag in self.document.find_all('tr')[1:101]:
            rank = int(tag.contents[0].string)
            word = tag.contents[1].string
            appearances = int(tag.contents[2].string.split('/')[0])
            perc_word_total = float(tag.contents[2].string.split('/')[1].strip(' %'))
            points = float(tag.contents[3].string)
            article_ids = [int(id) for id in tag.contents[4].string.split(',')]
            self.result.add_word(word, rank, appearances, perc_word_total, points, article_ids)

    pass


class WordEntry(object):
    def __init__(self, rank, appearances, perc_word_total, points, article_ids):
        """

        @param rank:
        @param appearances:
        @param perc_word_total:
        @param points:
        @param article_ids:
        """
        self.rank = rank
        self.appearances = appearances
        self.perc_word_total = perc_word_total
        self.points = points
        self.article_ids = article_ids

    pass


class Result(object):
    def __init__(self):
        self.words = {} # word: word_entry
        self.articles = {} # article_id: (source_id, link))
        self.sources = {} # name: id

    def _get_source_name(self, link):
        parsed_uri = urlparse(link)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return domain

    def add_article(self, article_link):
        """

        @param article_link:
        """
        source = self._get_source_name(article_link)
        try:
            source_id = self.sources[source]
        except KeyError:
            source_id = len(self.sources)
            self.sources[source] = source_id
        article_id = len(self.articles)
        self.articles[article_id] = (source_id, article_link)

    def add_word(self, word, rank, appearances, perc_word_total, points, article_ids):
        """

        @param word:
        @param rank:
        @param appearances:
        @param perc_word_total:
        @param points:
        @param article_ids:
        """
        self.words[word] = WordEntry(rank, appearances, perc_word_total, points, article_ids)

    def validate(self, validate_words_len=True):
        """

        @param validate_words_len:
        @raise MalformedWordsException:
        @raise MissingWordsException:
        """
        if validate_words_len and len(self.words) != 100:
            raise MissingWordsException(len(self.words))
        self.articles = {
            article_id: value
            for article_id, value in self.articles.items()
            for word_entry in self.words.values()
            if article_id in word_entry.article_ids
        }
        self.sources = {
            source_name: source_id
            for source_name, source_id in self.sources.items()
            for _article_id, value in self.articles.items()
            if source_id == value[0]
        }
        for word, word_entry in self.words.items():
            for article_id in word_entry.article_ids:
                if article_id not in self.articles:
                    raise MalformedWordsException(word, article_id)

    def __str__(self):
        return 'sources:{%d}\narticles:{%d}\n\nwords:{%d}' % (
            len(self.sources), len(self.articles), len(self.words))

    pass


def main(argv):
    for id in ['1', '2']:
        file = open('../{}test-data/example-log{}.html'.format(data_path_prefix, id), 'r')
        extractor = Extractor(file)
        data = extractor.extract_data()
    print("\\\\'http://www.nytimes.com/services/xml/rss/nyt/International.xml\\\\'".strip(r"\'"))

    r = Result()
    for i in range(100):
        r.add_article('http://www.nytimes.com/{}'.format(i))
    r.add_article('http://www.nytimes.com2/')
    r.add_word('qwe', 1, 1, 1, 1, [0, 1])
    r.add_word('qwa', 1, 1, 1, 1, [3, 2, 100])
    r.validate(validate_words_len=False)
    print(r)


if __name__ == '__main__':
    main(sys.argv)